package com.novak.nazar.gekkon.screens.request;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BaseFragment;
import com.novak.nazar.gekkon.models.Request;
import com.novak.nazar.gekkon.screens.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RequestFragment extends BaseFragment implements RequestView {

    @InjectPresenter RequestPresenter mPresenter;
    @BindView(R.id.archive) TextView archive;
    @BindView(R.id.actual) TextView actual;

    @BindView(R.id.archiveList) RecyclerView archiveList;
    @BindView(R.id.actualList) RecyclerView actualList;
    @BindView(R.id.noRequests) TextView noRequests;
    LinearLayoutManager llmArchiveList;
    LinearLayoutManager llmActualList;

    RequestAdapter actualAdapter;
    RequestAdapter archiveAdapter;
    int preLastActual;
    int preLastArchive;

    RequestAdapter.IClickListener mListener = request -> ((MainActivity)getActivity()).openRequestChat(request);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();

        actualAdapter = new RequestAdapter(mPresenter.getActualList());
        actualAdapter.setListener(mListener);
        actualList.setAdapter(actualAdapter);
        archiveAdapter = new RequestAdapter(mPresenter.getArchiveList());
        archiveAdapter.setListener(mListener);
        archiveList.setAdapter(archiveAdapter);


        actualList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int totalItemCount = llmActualList.getItemCount();
                int lastVisibleItem = llmActualList.findLastVisibleItemPosition();
                if (lastVisibleItem > totalItemCount - 10) {
                    if (preLastActual != lastVisibleItem) {
                        preLastActual = lastVisibleItem;
                        mPresenter.loadActualRequests(false);
                    }
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        archiveList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int totalItemCount = llmArchiveList.getItemCount();
                int lastVisibleItem = llmArchiveList.findLastVisibleItemPosition();
                if (lastVisibleItem > totalItemCount - 10) {
                    if (preLastArchive != lastVisibleItem) {
                        preLastArchive = lastVisibleItem;
                        mPresenter.loadArchiveRequests(false);
                    }
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });


        mPresenter.loadActualRequests(true);
    }

    @OnClick(R.id.add)
    protected void onClickAdd() {
        mPresenter.addRequest();
    }

    @OnClick(R.id.actual)
    protected void onClickActual() {
        selectArchive(false);
    }

    @OnClick(R.id.archive)
    protected void onClickArchive() {
        selectArchive(true);
    }

    @Override
    public void selectArchive(boolean status) {
        noRequests.setVisibility(View.GONE);
        if (getActivity() != null)
            if (status) {
                archive.setTextColor(getActivity().getResources().getColor(R.color.white));
                actual.setTextColor(getActivity().getResources().getColor(R.color.textColor));
                archive.setBackground(getActivity().getResources().getDrawable(R.drawable.background_selector_right_on));
                actual.setBackground(getActivity().getResources().getDrawable(R.drawable.background_selector_left_off));
                archiveList.setVisibility(View.VISIBLE);
                actualList.setVisibility(View.GONE);
                mPresenter.loadArchiveRequests(true);
            } else {
                archive.setTextColor(getActivity().getResources().getColor(R.color.textColor));
                actual.setTextColor(getActivity().getResources().getColor(R.color.white));
                archive.setBackground(getActivity().getResources().getDrawable(R.drawable.background_selector_right_off));
                actual.setBackground(getActivity().getResources().getDrawable(R.drawable.background_selector_left_on));
                archiveList.setVisibility(View.GONE);
                actualList.setVisibility(View.VISIBLE);
                mPresenter.loadActualRequests(true);
            }
    }

    @Override
    public void notifyArchive() {
        archiveAdapter.notifyDataSetChanged();
    }

    @Override
    public void notifyActual() {
        actualAdapter.notifyDataSetChanged();
    }

    @Override
    public void showNoRequests(String text) {
        noRequests.setVisibility(View.VISIBLE);
        noRequests.setText(text);
    }


    private void initRecyclerView() {
        archiveList.setHasFixedSize(true);
        llmArchiveList = new LinearLayoutManager(getActivity());
        llmArchiveList.setOrientation(LinearLayoutManager.VERTICAL);
        archiveList.setLayoutManager(llmArchiveList);

        actualList.setHasFixedSize(true);
        llmActualList = new LinearLayoutManager(getActivity());
        llmActualList.setOrientation(LinearLayoutManager.VERTICAL);
        actualList.setLayoutManager(llmActualList);
    }
}
