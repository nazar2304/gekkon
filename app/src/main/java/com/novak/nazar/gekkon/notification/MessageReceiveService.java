package com.novak.nazar.gekkon.notification;

import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.novak.nazar.gekkon.R;


public class MessageReceiveService extends FirebaseMessagingService {

    public static final String TAG = "myLog";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.w(TAG,"new Token: " + s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.w(TAG, "onMessageReceived: " );
        String title = remoteMessage.getNotification().getTitle();
        String body = remoteMessage.getNotification().getBody();

       /* Intent intentOpen = new Intent(this, MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intentOpen, PendingIntent.FLAG_ONE_SHOT);*/

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "chanel_id");


        mBuilder.setContentTitle(title);
        mBuilder.setContentText(body);
        mBuilder.setSmallIcon(R.drawable.add);
        mBuilder.setAutoCancel(true);
        mBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(sound);
        //mBuilder.setColor(Integer.parseInt(color));
      //  mBuilder.setContentIntent(pi);
        notificationManager.notify(String.valueOf(Long.toString(System.currentTimeMillis())), 1, mBuilder.build());
    }


}