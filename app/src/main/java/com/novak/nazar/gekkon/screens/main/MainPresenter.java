package com.novak.nazar.gekkon.screens.main;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.novak.nazar.gekkon.App;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BasePresenter;
import com.novak.nazar.gekkon.connection.Api;
import com.novak.nazar.gekkon.dagger.module.LocalStorage;
import com.novak.nazar.gekkon.models.User;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {

    @Inject LocalStorage mLocalStorage;
    @Inject Api mApi;

    MainPresenter() {
        App.getAppComponent().inject(this);
    }

    void loadUser() {
        User user = mLocalStorage.readObject("user", User.class);
        Log.w(TAG, "loadUser: " + user.getCity() );
        getViewState().setCity(user.getCity());
        getViewState().setName(user.getFName());
    }

    public void exit() {
        mLocalStorage.writeString("key", null);
        getViewState().exit();
    }

    void openRequests(){
        getViewState().openRequests();
    }

    public void saveToken(String token) {
        Map<String, String> queryInfo = new HashMap<>();
        queryInfo.put("key", mLocalStorage.readString("key", null));
        Log.w(TAG, "saveToken: " + token );
        queryInfo.put("token", token);
        Disposable disposable = mApi.sendToken(queryInfo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            Log.w(TAG, "saveToken: " + result.getResult() );
                        }
                        , throwable -> getViewState().showToast(R.string.unknown_exception));
        unsubscribeOnDestroy(disposable);
    }

    public void openActions() {
        getViewState().openActions();
    }
}
