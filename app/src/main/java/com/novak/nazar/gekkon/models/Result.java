package com.novak.nazar.gekkon.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("key")
    @Expose
    private String key;

    public String getResult() {
        return result;
    }


    public String getError() {
        return error;
    }

    public String getKey() {
        return key;
    }
}