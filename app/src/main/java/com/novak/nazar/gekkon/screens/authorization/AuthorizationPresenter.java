package com.novak.nazar.gekkon.screens.authorization;

import com.arellomobile.mvp.InjectViewState;
import com.novak.nazar.gekkon.App;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BasePresenter;
import com.novak.nazar.gekkon.connection.Api;
import com.novak.nazar.gekkon.dagger.module.LocalStorage;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


@InjectViewState
public class AuthorizationPresenter extends BasePresenter<AuthorizationView> {

    @Inject Api mApi;
    @Inject LocalStorage mLocalStorage;

    AuthorizationPresenter() {
        App.getAppComponent().inject(this);
    }

    void checkPhone(String phone) {
        Map<String, String> query = new HashMap<>();
        String clearPhone = phone.replace("+", "").replace("+", "")
                .replace(" ", "").replace("(", "")
                .replace(")", "").replace("-", "");
        clearPhone = clearPhone.substring(1, clearPhone.length());
        query.put("phone", clearPhone);

        Disposable disposable = mApi.authorization(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (result.getError() != null) {
                        getViewState().showToast(result.getError());
                    }
                    if (result.getResult() != null && result.getResult().equals("success")) {
                        getViewState().showPasswordField();
                    }
                }, throwable -> getViewState().showToast(R.string.unknown_exception));

        unsubscribeOnDestroy(disposable);

    }

    void authorization(String phone, String password) {
        Map<String, String> query = new HashMap<>();
        String clearPhone = phone.replace("+", "").replace("+", "")
                .replace(" ", "").replace("(", "")
                .replace(")", "").replace("-", "");
        clearPhone = clearPhone.substring(1, clearPhone.length());
        query.put("phone", clearPhone);
        query.put("password", password);

        Disposable disposable = mApi.authorization(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (result.getError() != null) {
                        getViewState().showToast(result.getError());
                    }
                    if (result.getKey() != null) {
                        mLocalStorage.writeString("key", result.getKey());
                        getInfo(result.getKey());
                    }
                }, throwable -> getViewState().showToast(R.string.unknown_exception));

        unsubscribeOnDestroy(disposable);

    }

    private void getInfo(String key) {
        Map<String, String> queryInfo = new HashMap<>();
        queryInfo.put("key", key);
        Disposable disposable = mApi.information(queryInfo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                            mLocalStorage.writeObject("user", user);
                            getViewState().showSuccess(user.getFName());
                        }
                        , throwable -> getViewState().showToast(R.string.unknown_exception));
        unsubscribeOnDestroy(disposable);
    }

}
