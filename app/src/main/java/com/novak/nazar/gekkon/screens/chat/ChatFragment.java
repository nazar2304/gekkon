package com.novak.nazar.gekkon.screens.chat;


import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BaseFragment;
import com.novak.nazar.gekkon.models.Request;
import com.novak.nazar.gekkon.screens.main.MainActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChatFragment extends BaseFragment implements ChatView {

    @InjectPresenter ChatPresenter mPresenter;
    @BindView(R.id.listView) RecyclerView listView;
    @BindView(R.id.number) TextView number;
    @BindView(R.id.type) TextView type;
    @BindView(R.id.status) TextView status;
    @BindView(R.id.message) TextView message;
    @BindView(R.id.msg) TextView msg;
    @BindView(R.id.info) View info;
    Request mRequest;

    MessageAdapter messageAdapter;
    LinearLayoutManager llm;

    private int preLast;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();

        mPresenter.setRequest(mRequest);

        messageAdapter = new MessageAdapter(mPresenter.getMessageList());

        listView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                int totalItemCount = llm.getItemCount();
                int lastVisibleItem = llm.findLastVisibleItemPosition();
                if (lastVisibleItem > totalItemCount - 10) {
                    if (preLast != lastVisibleItem) {
                        preLast = lastVisibleItem;
                        mPresenter.getMessages();
                    }
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        listView.setAdapter(messageAdapter);
        onHiddenChanged(false);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            mPresenter.getMessages();
        }
    }

    @OnClick(R.id.back)
    protected void onClickBack() {
        if (getActivity() != null)
            ((MainActivity) getActivity()).openRequests();
    }

    @OnClick(R.id.send)
    protected void onClickSend() {
        mPresenter.sendMessage(message.getText().toString());
    }


    public void setRequest(Request request) {
        mRequest = request;
    }

    private void initRecyclerView() {
        listView.setHasFixedSize(true);
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        llm.setReverseLayout(true);
        listView.setLayoutManager(llm);
    }

    @Override
    public void setRequestData(Request request) {
        number.setText(String.format(number.getResources().getString(R.string.placeholder_request_number), request.getNumber()));
        status.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(request.getStatus().getColor())));
        status.setText(request.getStatus().getName());
        type.setText(request.getType());
    }

    @Override
    public void notifyList() {
        messageAdapter.notifyDataSetChanged();
    }

    @Override
    public void clearMessageField() {
        message.setText("");
    }

    @Override
    public void showList(boolean status) {
        listView.setVisibility(status ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setMsg(String msg) {
        this.msg.setText(msg);
    }
}
