package com.novak.nazar.gekkon.screens.authorization;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.novak.nazar.gekkon.base.BaseView;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface AuthorizationView extends BaseView {
    void showPasswordField();

    void showSuccess(String name);
}
