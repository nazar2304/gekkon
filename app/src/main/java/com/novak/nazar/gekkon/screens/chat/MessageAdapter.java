package com.novak.nazar.gekkon.screens.chat;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BaseRecyclerAdapter;
import com.novak.nazar.gekkon.models.Message;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageAdapter extends BaseRecyclerAdapter<Message, MessageAdapter.ViewHolder> {

    MessageAdapter(List<Message> subCategories) {
        this.items = subCategories;
    }

    @NonNull
    @Override
    public MessageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MessageAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MessageAdapter.ViewHolder holder, int position) {
        holder.bindView(getItem(position));
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text) TextView text;
        @BindView(R.id.receive) View receive;
        @BindView(R.id.send) View send;
        @BindView(R.id.date) TextView date;
        @BindView(R.id.name) TextView name;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindView(Message message) {
            text.setText(message.getText());
            name.setText(message.getFrom());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
            if (message.getDatetime() != null)
                try {
                    Date createDate = simpleDateFormat.parse(message.getDatetime());
                    SimpleDateFormat newSimpleDateFormat = new SimpleDateFormat("dd.MM.yyyy в hh:mm", Locale.ENGLISH);
                    date.setText(newSimpleDateFormat.format(createDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            else {
                date.setText("");
            }
            text.setBackgroundResource(message.getType().equals("outbox") ? R.drawable.background_message_send : R.drawable.background_message_receive);
            send.setVisibility(message.getType().equals("outbox") ? View.VISIBLE : View.GONE);
            receive.setVisibility(!message.getType().equals("outbox") ? View.VISIBLE : View.GONE);
        }
    }
}
