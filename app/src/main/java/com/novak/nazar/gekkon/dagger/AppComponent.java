package com.novak.nazar.gekkon.dagger;


import com.novak.nazar.gekkon.dagger.module.ApiModule;
import com.novak.nazar.gekkon.dagger.module.ContextModule;
import com.novak.nazar.gekkon.dagger.module.LocalStorageModule;
import com.novak.nazar.gekkon.screens.authorization.AuthorizationPresenter;
import com.novak.nazar.gekkon.screens.chat.ChatPresenter;
import com.novak.nazar.gekkon.screens.main.MainPresenter;
import com.novak.nazar.gekkon.screens.main.MainFragmentPresenter;
import com.novak.nazar.gekkon.screens.request.RequestPresenter;
import com.novak.nazar.gekkon.screens.start.StartPresenter;
import com.novak.nazar.gekkon.screens.register.RegisterPresenter;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {ContextModule.class, ApiModule.class, LocalStorageModule.class})
public interface AppComponent {

    void inject(StartPresenter example);

    void inject(AuthorizationPresenter example);

    void inject(RegisterPresenter example);

    void inject(MainPresenter example);

    void inject(MainFragmentPresenter example);

    void inject(RequestPresenter example);

    void inject(ChatPresenter example);

}

