package com.novak.nazar.gekkon.screens.chat;

import com.novak.nazar.gekkon.base.BaseView;
import com.novak.nazar.gekkon.models.Request;

public interface ChatView extends BaseView {

    void setRequestData(Request request);

    void notifyList();

    void clearMessageField();

    void showList(boolean status);

    void setMsg(String msg);
}
