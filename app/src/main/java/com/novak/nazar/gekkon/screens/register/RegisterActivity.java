package com.novak.nazar.gekkon.screens.register;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class RegisterActivity extends BaseActivity implements RegisterView {

    @InjectPresenter RegisterPresenter mPresenter;
    @BindView(R.id.register) View register;
    @BindView(R.id.phone) EditText phone;
    @BindView(R.id.containerPhone) View containerPhone;
    @BindView(R.id.containerSuccess) View containerSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        unbinder = ButterKnife.bind(this);
    }

    @OnClick(R.id.back)
    protected void onClickBack() {
        onBackPressed();
    }

    @OnClick(R.id.register)
    protected void onClickRegister() {
        Log.w(TAG, "onClickRegister: " + phone.getText());
        mPresenter.register(phone.getText().toString());
        hideKeyboard();
    }

    @OnTextChanged(value = R.id.phone, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void onChangePhone(CharSequence text) {
        if (text.length() > 18 && !text.toString().contains("_")) {
            return;
        }
        register.setVisibility(text.length() == 18 ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void showSuccess() {
        containerPhone.setVisibility(View.GONE);
        containerSuccess.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.mainContainer)
    protected void clickMainContainer(){
        hideKeyboard();
    }

    @OnClick(R.id.call)
    protected void onClickCall(){
        String phone = "+380990348354";
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }
}
