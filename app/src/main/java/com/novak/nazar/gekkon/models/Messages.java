package com.novak.nazar.gekkon.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Messages {

    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("requests")
    @Expose
    private List<Message> messages = null;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> requests) {
        this.messages = requests;
    }

}