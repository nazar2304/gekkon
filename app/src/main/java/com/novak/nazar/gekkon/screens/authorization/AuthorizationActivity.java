package com.novak.nazar.gekkon.screens.authorization;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BaseActivity;
import com.novak.nazar.gekkon.screens.main.MainActivity;
import com.novak.nazar.gekkon.views.AsteriskPasswordTransformationMethod;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class AuthorizationActivity extends BaseActivity implements AuthorizationView {

    @InjectPresenter AuthorizationPresenter mPresenter;
    @BindView(R.id.bContinue) View bContinue;
    @BindView(R.id.enter) View enter;
    @BindView(R.id.containerPassword) View containerPassword;
    @BindView(R.id.containerPhone) View containerPhone;
    @BindView(R.id.containerSuccess) View containerSuccess;
    @BindView(R.id.phone) EditText phone;
    @BindView(R.id.password) EditText password;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.progress) ImageView progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);
        unbinder = ButterKnife.bind(this);
        password.setTransformationMethod(new AsteriskPasswordTransformationMethod());

        AnimationDrawable animationDrawable = (AnimationDrawable) progress.getDrawable();
        animationDrawable.start();
    }

    @OnClick(R.id.back)
    protected void onClickBack() {
        if(containerPassword.getVisibility() == View.VISIBLE){
            containerPassword.setVisibility(View.GONE);
            password.setText("");
            containerPhone.setVisibility(View.VISIBLE);
            return;
        }
        onBackPressed();
    }


    @OnTextChanged(value = R.id.phone, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void onChangePhone(CharSequence text) {
        if (text.length() > 18 && !text.toString().contains("_")) {
            return;
        }
        bContinue.setVisibility(text.length() == 18 ? View.VISIBLE : View.INVISIBLE);
        if(text.length() == 18){
            hideKeyboard();
        }
    }

    @OnTextChanged(value = R.id.password, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void onChangePassword(CharSequence text) {
        enter.setVisibility(text.length() > 5 ? View.VISIBLE : View.INVISIBLE);
    }

    @OnClick(R.id.bContinue)
    protected void onClickContinue(){
        mPresenter.checkPhone(phone.getText().toString());
    }

    @OnClick(R.id.enter)
    protected void onClickEnter(){
        mPresenter.authorization(phone.getText().toString(), password.getText().toString());
    }



    @Override
    public void showPasswordField() {
        containerPhone.setVisibility(View.GONE);
        containerPassword.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSuccess(String name) {
        this.name.setText(name);
        containerSuccess.setVisibility(View.VISIBLE);
        hideKeyboard();
        new Handler().postDelayed(() -> startActivity(new Intent(AuthorizationActivity.this, MainActivity.class)), 500);
    }

    @OnClick(R.id.mainContainer)
    protected void clickMainContainer(){
        hideKeyboard();
    }

    @OnClick(R.id.call)
    protected void onClickCall(){
        String phone = "+380990348354";
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }
}
