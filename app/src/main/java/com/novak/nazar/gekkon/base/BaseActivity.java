package com.novak.nazar.gekkon.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.IdRes;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;

import butterknife.Unbinder;

public abstract class BaseActivity extends MvpAppCompatActivity implements BaseView {

    public static final String TAG = "myLog";

    protected View progress;
    protected Unbinder unbinder;

    @SuppressLint("ResourceType")
    public void showToast(@IdRes int id) {
        Toast.makeText(this, id, Toast.LENGTH_SHORT).show();
    }

    public void showToast(String error) {
        Log.w(TAG, "showToast: " + error );
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress(boolean status) {
        progress.setVisibility(status ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    public  void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
