package com.novak.nazar.gekkon.screens.start;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BaseActivity;
import com.novak.nazar.gekkon.models.Images;
import com.novak.nazar.gekkon.screens.authorization.AuthorizationActivity;
import com.novak.nazar.gekkon.screens.main.MainActivity;
import com.novak.nazar.gekkon.screens.register.RegisterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartActivity extends BaseActivity implements StartView {

    @InjectPresenter StartPresenter mainPresenter;

    @BindView(R.id.viewPager) ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        unbinder = ButterKnife.bind(this);
        mainPresenter.checkAuthorization();
        mainPresenter.getBanners();
    }

    @OnClick(R.id.register)
    protected void onClickRegister() {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    @OnClick(R.id.alreadyRegistered)
    protected void onClickAlreadyRegister() {
        startActivity(new Intent(this, AuthorizationActivity.class));
    }

    @Override
    public void showBanners(Images images) {
        InfiniteAdapter adapter = new InfiniteAdapter(this, images.getImages());
        mPager.setAdapter(adapter);
        mPager.setCurrentItem(images.getImages().size() * 100);
    }

    @Override
    public void openMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
