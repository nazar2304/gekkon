package com.novak.nazar.gekkon.base;

import android.support.v7.widget.RecyclerView;

import java.util.List;


public abstract class BaseRecyclerAdapter<T, V extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<V> {

    protected List<T> items;


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    protected T getItem(int position){
        return items.get(position);
    }
}
