package com.novak.nazar.gekkon.connection;

import com.novak.nazar.gekkon.models.Images;
import com.novak.nazar.gekkon.models.Message;
import com.novak.nazar.gekkon.models.Messages;
import com.novak.nazar.gekkon.models.Request;
import com.novak.nazar.gekkon.models.Requests;
import com.novak.nazar.gekkon.models.Result;
import com.novak.nazar.gekkon.models.User;

import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

import static com.novak.nazar.gekkon.connection.ApiConstant.*;


public interface Api {

    @FormUrlEncoded
    @POST(REGISTER)
    Observable<Result> register(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(BANNERS)
    Observable<Images> banners(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(AUTHORIZATION)
    Observable<Result> authorization(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(INFORMATION)
    Observable<User> information(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(REQUESTS)
    Observable<Requests> getRequests(@FieldMap Map<String, String> fields);


    @FormUrlEncoded
    @POST(ADD_REQUEST)
    Observable<Request> addRequest(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(GET_MESSAGES)
    Observable<Messages> getMessages(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(SEND_MESSAGES)
    Observable<Result> sendMessage(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(SEND_TOKEN)
    Observable<Result> sendToken(@FieldMap Map<String, String> fields);
}
