package com.novak.nazar.gekkon.screens.request;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BaseRecyclerAdapter;
import com.novak.nazar.gekkon.models.Request;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestAdapter extends BaseRecyclerAdapter<Request, RequestAdapter.ViewHolder> {

    RequestAdapter(List<Request> subCategories) {
        this.items = subCategories;
    }
    private IClickListener mListener;

    @NonNull
    @Override
    public RequestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RequestAdapter.ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_request, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RequestAdapter.ViewHolder holder, int position) {
        holder.bindView(getItem(position));
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.number) TextView number;
        @BindView(R.id.type) TextView type;
        @BindView(R.id.status) TextView status;
        @BindView(R.id.date) TextView date;
        @BindView(R.id.container) View container;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindView(Request request) {

            number.setText(String.format(number.getResources().getString(R.string.placeholder_request_number), request.getNumber()));
            status.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(request.getStatus().getColor())));
            status.setText(request.getStatus().getName());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
            if (request.getDatetimeCreate() != null)
                try {
                    Date createDate = simpleDateFormat.parse(request.getDatetimeCreate());
                    SimpleDateFormat newSimpleDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
                    date.setText(newSimpleDateFormat.format(createDate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                else {
                date.setText("");
            }
            type.setText(request.getType());

            container.setOnClickListener(v -> mListener.onClick(request));
        }
    }

    void setListener(IClickListener listener) {
        mListener = listener;
    }

    interface IClickListener{
        void onClick(Request request);
    }
}
