package com.novak.nazar.gekkon.screens.main;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BaseFragment;
import com.novak.nazar.gekkon.models.Images;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainFragment extends BaseFragment implements MainFragmentView {

    @InjectPresenter MainFragmentPresenter mPresenter;
    @BindView(R.id.viewPager) ViewPager mPager;



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.getBanners();
    }

    @Override
    public void showBanners(Images images) {
        InfiniteAdapter adapter = new InfiniteAdapter(getActivity(), images.getImages());
        mPager.setAdapter(adapter);
        mPager.setCurrentItem(images.getImages().size() * 100);
    }

    @OnClick(R.id.requests)
    protected void onClickRequests(){
        if(getActivity() != null){
            ((MainActivity)getActivity()).mPresenter.openRequests();
        }
    }
}
