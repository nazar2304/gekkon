package com.novak.nazar.gekkon.screens.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.firebase.iid.FirebaseInstanceId;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BaseActivity;
import com.novak.nazar.gekkon.models.Request;
import com.novak.nazar.gekkon.screens.authorization.AuthorizationActivity;
import com.novak.nazar.gekkon.screens.chat.ChatFragment;
import com.novak.nazar.gekkon.screens.request.RequestFragment;
import com.novak.nazar.gekkon.screens.start.StartActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainView {

    @InjectPresenter MainPresenter mPresenter;
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawer;
    @BindView(R.id.name) TextView mName;
    @BindView(R.id.city) TextView mCity;
    @BindView(R.id.actions) TextView actions;
    @BindView(R.id.requests) TextView requests;
    public ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);


        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toggle = new ActionBarDrawerToggle(this, mDrawer, null, R.string.drawer_open, R.string.drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.drawer);
        mDrawer.addDrawerListener(toggle);
        toggle.setDrawerSlideAnimationEnabled(true);
        toggle.syncState();

        mPresenter.loadUser();

        changeFragment(new MainFragment(), "MainFragment");

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(MainActivity.this, instanceIdResult -> {
            mPresenter.saveToken(instanceIdResult.getToken());
        });
    }


    @OnClick(R.id.requests)
    protected void onClickRequest() {
        mPresenter.openRequests();
    }


    @OnClick(R.id.actions)
    protected void onClickAction() {
        mPresenter.openActions();
    }

    public void changeFragment(Fragment fragment, String tag) {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
/*
        List<Fragment> fragments = fm.getFragments();
        for (Fragment f : fragments) {
            if (f != null && f.isVisible())
                ft.hide(f);
        }

        Fragment fragmentOld = fm.findFragmentByTag(tag);
        if (fragmentOld != null) {
            fragment = fragmentOld;
            ft.show(fragment).commit();
        } else {
            ft.add(R.id.fragment_container, fragment, tag).show(fragment).commit();
        }*/
        ft.replace(R.id.fragment_container, fragment).commit();
        mDrawer.closeDrawer(Gravity.START);
    }

    @OnClick(R.id.exit)
    protected void onClickExit() {
        mPresenter.exit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu != null) menu.clear();
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.call:
                String phone = "+380990348354";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
                break;
            default:
                mDrawer.openDrawer(Gravity.START);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setName(String name) {
        mName.setText(name);
    }

    @Override
    public void setCity(String city) {
        Log.w(TAG, "setCity: ");
        mCity.setText(city);
    }

    @Override
    public void exit() {
        Intent mStartActivity = new Intent(this, StartActivity.class);
        mStartActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mStartActivity);
    }

    @Override
    public void openRequests() {
        requests.setCompoundDrawablesWithIntrinsicBounds(0,0,  R.drawable.next, 0);
        actions.setCompoundDrawablesWithIntrinsicBounds(0,0,0, 0);
        changeFragment(new RequestFragment(), "RequestFragment");
    }

    @Override
    public void openActions() {
        requests.setCompoundDrawablesWithIntrinsicBounds(0,0, 0, 0);
        actions.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.next, 0);
        changeFragment(new MainFragment(), "MainFragment");
    }

    @Override
    public void openRequestChat(Request request) {
        ChatFragment chatFragment = new ChatFragment();
        chatFragment.setRequest(request);
        changeFragment(chatFragment, "ChatFragment");
    }
}
