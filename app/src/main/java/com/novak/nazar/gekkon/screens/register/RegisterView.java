package com.novak.nazar.gekkon.screens.register;

import com.novak.nazar.gekkon.base.BaseView;

public interface RegisterView extends BaseView {
    void showSuccess();
}
