package com.novak.nazar.gekkon.screens.main;

import com.arellomobile.mvp.InjectViewState;
import com.novak.nazar.gekkon.App;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BasePresenter;
import com.novak.nazar.gekkon.connection.Api;
import com.novak.nazar.gekkon.dagger.module.LocalStorage;
import com.novak.nazar.gekkon.helpers.Ratio;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class MainFragmentPresenter extends BasePresenter<MainFragmentView> {

    @Inject Api mApi;
    @Inject LocalStorage mLocalStorage;

    MainFragmentPresenter() {
        App.getAppComponent().inject(this);
    }

    void getBanners() {

        Map<String, String> query = new HashMap<>();
        query.put("window", "info");
        query.put("ratio", Ratio.getScreenSize(App.getApp()));

        Disposable disposable = mApi.banners(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> getViewState().showBanners(result)
                        , throwable -> getViewState().showToast(R.string.unknown_exception));

        unsubscribeOnDestroy(disposable);
    }
}
