package com.novak.nazar.gekkon.screens.request;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.novak.nazar.gekkon.base.BaseView;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface RequestView extends BaseView {

    void selectArchive(boolean status);

    void notifyArchive();

    void notifyActual();

    void showNoRequests(String text);
}
