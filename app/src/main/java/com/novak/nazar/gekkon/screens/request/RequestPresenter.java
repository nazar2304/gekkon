package com.novak.nazar.gekkon.screens.request;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.novak.nazar.gekkon.App;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BasePresenter;
import com.novak.nazar.gekkon.connection.Api;
import com.novak.nazar.gekkon.dagger.module.LocalStorage;
import com.novak.nazar.gekkon.models.Request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class RequestPresenter extends BasePresenter<RequestView> {

    @Inject Api mApi;
    @Inject LocalStorage mLocalStorage;
    private List<Request> actualList = new ArrayList<>();
    private List<Request> archiveList = new ArrayList<>();
    private boolean actualLoadAll = false;
    private boolean archiveLoadAll = false;
    private boolean isLoadingActual;
    private boolean isLoadingArchive;

    public List<Request> getActualList() {
        return actualList;
    }

    public List<Request> getArchiveList() {
        return archiveList;
    }

    RequestPresenter() {
        App.getAppComponent().inject(this);
    }

    void loadActualRequests(boolean isReload) {

        if (isReload) {
            actualList.clear();
            getViewState().notifyActual();
            actualLoadAll = false;
            isLoadingActual = false;
        }

        if (actualLoadAll || isLoadingActual) {
            return;
        }
        isLoadingActual = true;
        Map<String, String> query = new HashMap<>();
        query.put("key", mLocalStorage.readString("key", ""));
        query.put("archived", "0");
        query.put("page", String.valueOf(actualList.size() / 10 + 1));

        Disposable disposable = mApi.getRequests(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (result.getRequests().size() < 10) {
                                actualLoadAll = true;
                            }
                         //   actualList.addAll(result.getRequests());
                            for(int i = 0; i < result.getRequests().size(); i++){
                                if(!actualList.contains(result.getRequests().get(i))){
                                    actualList.add(result.getRequests().get(i));
                                }
                            }
                            getViewState().notifyActual();
                            isLoadingActual = false;
                            if(actualList.size() == 0){
                                getViewState().showNoRequests(result.getMsg());
                            }
                        }
                        , throwable -> {
                            getViewState().showToast(R.string.unknown_exception);
                            isLoadingActual = false;
                        });

        unsubscribeOnDestroy(disposable);

    }

    void addRequest() {
        Map<String, String> query = new HashMap<>();
        query.put("key", mLocalStorage.readString("key", ""));

        Disposable disposableArchived = mApi.addRequest(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            actualList.add(0, result);
                            getViewState().notifyActual();

                        }
                        , throwable -> getViewState().showToast(R.string.unknown_exception));

        unsubscribeOnDestroy(disposableArchived);
    }

    void loadArchiveRequests(boolean isReload) {
        if (isReload) {
            archiveList.clear();
            getViewState().notifyArchive();
            archiveLoadAll = false;
            isLoadingArchive = false;
        }

        if (archiveLoadAll || isLoadingArchive) {
            return;
        }
        isLoadingArchive = true;
        Map<String, String> query = new HashMap<>();
        query.put("key", mLocalStorage.readString("key", ""));
        query.put("archived", "1");
        query.put("page", "1");

        Disposable disposableArchived = mApi.getRequests(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (result.getRequests().size() < 10) {
                                archiveLoadAll = true;
                            }
                            for(int i = 0; i < result.getRequests().size(); i++){
                                if(!archiveList.contains(result.getRequests().get(i))){
                                    archiveList.add(result.getRequests().get(i));
                                }
                            }
                            getViewState().notifyArchive();
                            isLoadingArchive = false;
                            if(archiveList.size() == 0){
                                getViewState().showNoRequests(result.getMsg());
                            }
                        }
                        , throwable -> {
                            getViewState().showToast(R.string.unknown_exception);
                            isLoadingArchive = false;
                        });

        unsubscribeOnDestroy(disposableArchived);
    }
}
