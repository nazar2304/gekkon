package com.novak.nazar.gekkon.screens.register;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.novak.nazar.gekkon.App;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BasePresenter;
import com.novak.nazar.gekkon.connection.Api;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class RegisterPresenter extends BasePresenter<RegisterView> {

    @Inject Api mApi;

    RegisterPresenter() {
        App.getAppComponent().inject(this);
    }

    void register(String phone) {
        Map<String, String> query = new HashMap<>();
        String clearPhone = phone.replace("+", "").replace("+", "")
                .replace(" ", "").replace("(", "")
                .replace(")", "").replace("-", "");
        clearPhone = clearPhone.substring(1, clearPhone.length());
        query.put("phone", clearPhone);
        Disposable disposable = mApi.register(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                            if (result.getError() != null) {
                                getViewState().showToast(result.getError());
                            }
                            if (result.getResult() != null && result.getResult().equals("success")) {
                                getViewState().showSuccess();
                            }
                        }, throwable -> getViewState().showToast(R.string.unknown_exception)
                );

        unsubscribeOnDestroy(disposable);
    }
}
