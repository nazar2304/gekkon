package com.novak.nazar.gekkon.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Request {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("datetime_create")
    @Expose
    private String datetimeCreate;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("archived")
    @Expose
    private String archived;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDatetimeCreate() {
        return datetimeCreate;
    }

    public void setDatetimeCreate(String datetimeCreate) {
        this.datetimeCreate = datetimeCreate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getArchived() {
        return archived;
    }

    public void setArchived(String archived) {
        this.archived = archived;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return Objects.equals(number, request.number);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}