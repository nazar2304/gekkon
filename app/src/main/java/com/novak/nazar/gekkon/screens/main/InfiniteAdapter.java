package com.novak.nazar.gekkon.screens.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;


class InfiniteAdapter extends android.support.v4.view.PagerAdapter {

    private Context context;
    private List<String> banners;

    InfiniteAdapter(Context context, List<String> banners){
        this.context = context;
        this.banners = banners;
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView img = new ImageView(context);
        img.setScaleType(ImageView.ScaleType.FIT_XY);
        container.addView(img);
        if(position >= banners.size()){
            position = position%banners.size();
        }
        Glide.with(context).load(banners.get(position)).into(img);

        return img;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }

}