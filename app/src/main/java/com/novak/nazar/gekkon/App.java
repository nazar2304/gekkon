package com.novak.nazar.gekkon;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;

import com.novak.nazar.gekkon.dagger.AppComponent;
import com.novak.nazar.gekkon.dagger.DaggerAppComponent;
import com.novak.nazar.gekkon.dagger.module.ContextModule;

public class App extends Application {

    private static AppComponent sAppComponent;
    private static App app;

    @Override
    public void onCreate() {
        super.onCreate();

        sAppComponent = DaggerAppComponent.builder().contextModule(new ContextModule(this)).build();
        app = this;

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel("chanel_id", "chanel_id", NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription("Gekkon");
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(mChannel);
        }
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public static App getApp() {
        return app;
    }
}
