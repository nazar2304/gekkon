package com.novak.nazar.gekkon.screens.chat;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.novak.nazar.gekkon.App;
import com.novak.nazar.gekkon.R;
import com.novak.nazar.gekkon.base.BasePresenter;
import com.novak.nazar.gekkon.connection.Api;
import com.novak.nazar.gekkon.dagger.module.LocalStorage;
import com.novak.nazar.gekkon.models.Message;
import com.novak.nazar.gekkon.models.Request;
import com.novak.nazar.gekkon.models.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class ChatPresenter extends BasePresenter<ChatView> {

    @Inject Api mApi;
    @Inject LocalStorage mLocalStorage;
    private boolean isLoading;
    private boolean loadAll;

    private List<Message> mMessageList = new ArrayList<>();

    List<Message> getMessageList() {
        return mMessageList;
    }

    private Request mRequest;

    ChatPresenter() {
        App.getAppComponent().inject(this);
    }

    public void setRequest(Request request) {
        mRequest = request;
        getViewState().setRequestData(request);
    }

    void getMessages() {
        if (isLoading || loadAll) {
            return;
        }

        Map<String, String> query = new HashMap<>();

        query.put("key", mLocalStorage.readString("key", ""));
        query.put("application_id", String.valueOf(mRequest.getNumber()));
        query.put("page", String.valueOf(mMessageList.size() / 10 + 1));

        isLoading = true;

        Disposable disposable = mApi.getMessages(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (result.getMessages().size() < 10) {
                        loadAll = true;
                    }

                    Log.w(TAG, "getMessages: " + result.getMessages().size());
                    mMessageList.addAll(result.getMessages());
                    getViewState().notifyList();
                    isLoading = false;
                    getViewState().showList(mMessageList.size() != 0);
                    getViewState().setMsg(result.getMsg());
                }, throwable -> {
                    Log.w(TAG, "getMessages: " + throwable);
                    isLoading = false;
                });

        unsubscribeOnDestroy(disposable);
    }


    void sendMessage(String message) {

        Map<String, String> query = new HashMap<>();

        query.put("key", mLocalStorage.readString("key", ""));
        query.put("application_id", String.valueOf(mRequest.getNumber()));
        query.put("msg", message);

        getViewState().clearMessageField();
        Disposable disposable = mApi.sendMessage(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (result.getResult().equals("success")) {
                        Date date = new Date(System.currentTimeMillis());
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
                        Message m = new Message();
                        m.setType("outbox");
                        m.setDatetime(simpleDateFormat.format(date));
                        m.setFrom(mLocalStorage.readObject("user", User.class).getFName() + " " + mLocalStorage.readObject("user", User.class).getLName());
                        m.setText(message);
                        mMessageList.add(0, m);
                        getViewState().showList(mMessageList.size() != 0);
                        getViewState().notifyList();
                    } else {
                        getViewState().showToast(R.string.unknown_exception);
                    }

                }, throwable -> {
                    getViewState().showToast(R.string.unknown_exception);
                });

        unsubscribeOnDestroy(disposable);
    }
}
