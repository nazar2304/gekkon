package com.novak.nazar.gekkon.screens.main;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.novak.nazar.gekkon.base.BaseView;
import com.novak.nazar.gekkon.models.Request;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface MainView extends BaseView {
    void setName(String name);

    void setCity(String city);

    void exit();

    void openRequests();

    void openActions();

    void openRequestChat(Request request);
}
