package com.novak.nazar.gekkon.screens.start;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.novak.nazar.gekkon.base.BaseView;
import com.novak.nazar.gekkon.models.Images;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface StartView extends BaseView {
    void showBanners(Images images);

    void openMain();
}
