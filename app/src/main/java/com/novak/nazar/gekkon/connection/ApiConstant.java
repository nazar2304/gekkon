package com.novak.nazar.gekkon.connection;

public class ApiConstant {
    public static final String URL = "http://mobile.gekkon24.kz/";
    static final String REGISTER = "/api/accounts/registration.php";
    static final String BANNERS = "/api/start/banners.php";
    static final String AUTHORIZATION = "/api/accounts/authorization.php";
    static final String INFORMATION = "/api/accounts/information.php";

    static final String REQUESTS = "/api/applications/list.php";
    static final String ADD_REQUEST = "/api/applications/add.php";
    static final String GET_MESSAGES = "/api/applications/get-messages.php";
    static final String SEND_MESSAGES = "/api/applications/send-client-message.php";
    static final String SEND_TOKEN = "/api/accounts/token.php?";
}
